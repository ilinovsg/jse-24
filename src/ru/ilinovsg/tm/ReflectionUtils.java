package ru.ilinovsg.tm;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class ReflectionUtils {
    private ReflectionUtils() {
    }

    public static void getClassFields(List<Object> objects) throws IOException {
        try(BufferedWriter buffer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("text.csv"), StandardCharsets.UTF_8))) {
            for (Object object : objects) {
                Class<?> clazz = object.getClass();
                if (clazz.getTypeName() != Person.class.getTypeName()) {
                    throw new IllegalArgumentException();
                }
                buffer.write(fieldIterator(clazz.getDeclaredFields(), object));
                if (objects.indexOf(object) < objects.size() - 1) {
                    buffer.write("\n");
                }
             }
        }
    }

    private static String fieldIterator(Field[] fields, Object object) {
        String result = "";
        for (Field field : fields){
            try {
                field.setAccessible(true);
                if (field.get(object) != null) {
                    result += field.get(object).toString() + (field.getName() == "email" ? "" : ", " );
                }
                else {
                    result += (field.getName() == "email" ? "" : ", " );
                }
            } catch (IllegalAccessException e) {
            }
        }
        return result;
    }
}
